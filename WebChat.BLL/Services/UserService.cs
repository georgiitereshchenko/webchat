﻿
using System;
using System.Collections.Generic;
using System.Text;
using WebChat.BLL.DTOModels;
using WebChat.BLL.Interfaces;
using WebChat.Domain.Models;
using WebChat.Domain.Repositories;

namespace WebChat.BLL.Services
{
    public class UserService : IUserService
    {
        UserRepository userR = new UserRepository();
        /// <summary>
        /// Регистрация пользователя
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public Result<bool> RegistredUser(UserDTO userDTO)
        {
            var errors = new StringBuilder();

            if (userDTO == null || userDTO.Login == null)
            {
                errors.Append("Все поля должны быть заполнины \n");
            }
            if (userR.SearchLoginUser(userDTO.Login))
            {
                errors.Append("Такой пользователь уже есть \n");
            }
            var errorMessage = errors.ToString();
            if (!string.IsNullOrEmpty(errorMessage))
                return new Result<bool> { IsSuccess = false, Message = errorMessage };

            try
            {

                var user = new User { Login = userDTO.Login, Pass = userDTO.Pass };
                userR.Create(user);
                return new Result<bool>
                {
                    IsSuccess = true,
                    Message = "",
                    ResultData = true
                };
            }
            catch (Exception ex)
            {
                return new Result<bool>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Авторизация пользователя
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        
        public Result<bool> AvtorizeitUser(UserDTO userDTO)
        {

            var errors = new StringBuilder();
            
            if (userDTO == null || userDTO.Login == null || userDTO.Pass == null)
            {
                errors.Append("Все поля должны быть заполнины \n");
                var errorMessage = errors.ToString();
                return new Result<bool> { IsSuccess = false, Message = errorMessage };

            }

            if (!userR.SearchUserLoginAndPass(userDTO.Login, userDTO.Pass))
            {
                errors.Append("Такого пользователя нет \n");
                var errorMessage = errors.ToString();
                return new Result<bool> { IsSuccess = false, Message = errorMessage };
            }
           
            try
            {
                return new Result<bool>
                {
                    IsSuccess = true,
                    Message = "",
                    ResultData = true
                };
            }
            catch (Exception ex)
            {
                return new Result<bool>
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Проверка есть ли такой пользователь
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public User SersUser(UserDTO userDTO) => userR.SearchUserModel(userDTO.Login);
        
        /// <summary>
        /// Возращает всь список пользователей
        /// </summary>
        /// <returns></returns>
        public List<UserDTO> GetAllDTOUser()
        {
            var userDTOList = new List<UserDTO>();
            var userRepository = new UserRepository();
            var users = new List<User>();
            users = userRepository.GetAllUsers();
            foreach (var user in users)
            {
                UserDTO userDTO = new UserDTO() { Id = user.Id, Login = user.Login };
                userDTOList.Add(userDTO);
            }

            return userDTOList;
        }
    }
}
